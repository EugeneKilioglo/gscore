/** @type {import('next').NextConfig} */
const path = require("path");

const nextConfig = {
  reactStrictMode: true,
  experimental: {
    appDir: true,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "src/app/assets/styles")],
    prependData: `@import "./mixins/mixins.scss";`,
  },
};

module.exports = nextConfig;
