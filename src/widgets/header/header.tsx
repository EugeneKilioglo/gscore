import { FC } from "react";
import { useClasses } from "./lib/use-classes";
import { SvgLogo } from "@/shared/icons/components/logo";
import { ButtonPrimary } from "@/shared/ui/buttons/button-primary/primary";
import Link from "next/link";
import { Dropdown } from "@/shared/ui/dropdown/dropdown";
import { menuItems } from "@/shared/mocks/dropmenu-items";

export interface HeaderProps {
  authUserName?: string;
  isAuthUser?: boolean;
  className?: string;
}

export const Header: FC<HeaderProps> = ({ className, isAuthUser = false, authUserName }) => {
  
  const { cnRoot, cnLogo, cnButton, cnLeftWrapper, cnRightWrapper, cnAuthWrapper, cnLinkToSubs } = useClasses({
    className
  });

  return (
    <div className={cnRoot}>
      <div className={cnLeftWrapper}>
      <span className={cnLogo}><SvgLogo width='100%' height='100%'/></span>
      </div>
      <div className={cnRightWrapper}>
      {isAuthUser ?  <div className={cnAuthWrapper}>
        <Link href="/subcriptions" className={cnLinkToSubs}>{'My subscriptions'}</Link>
        <Dropdown title={authUserName} menuItems={menuItems}/>
      </div>: 
      <ButtonPrimary className={cnButton}>Get Gscore</ButtonPrimary>
      }
      </div>
    </div>
  );
};