import classNames from "classnames/bind";
import classes from "../header.module.scss";
import { HeaderProps } from "../header";

const cn = classNames.bind(classes);

type PickedBaseProps = Pick<
HeaderProps,
  "className"
>;

export const useClasses = ({
  className,
}: PickedBaseProps) => {
  const cnRoot = cn('root', className);
  const cnLogo = cn('logo');
  const cnButton = cn('button');
  const cnRightWrapper = cn('right-wrapper');
  const cnLeftWrapper = cn('left-wrapper');
  const cnAuthWrapper = cn('auth-wrapper');
  const cnLinkToSubs = cn('link-to-subs');
  
  return {
    cnRoot,
    cnLogo,
    cnButton,
    cnRightWrapper,
    cnLeftWrapper,
    cnAuthWrapper,
    cnLinkToSubs
  };
};
