import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Head>
        <title>Gscore</title>
        <meta name="description" content="Generated by me" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Link href="/ui-kit/ui-kit" style={{'color': 'white', 'fontSize': '40px', 'padding': '40px', 'display': 'block'}}>{"Ui Kit"}</Link>
      </main>
    </>
  );
}