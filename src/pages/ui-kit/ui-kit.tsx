import { InputStatus } from "@/shared/types";
import styles from "./styles.module.scss";
import { ButtonPrimary } from "@/shared/ui/buttons/button-primary/primary";
import { ButtonSecondary } from "@/shared/ui/buttons/button-secondary/secondary";
import { Checkbox } from "@/shared/ui/checkbox/checkbox";
import { Input } from "@/shared/ui/input/input";
import { Header } from "@/widgets/header/header";
import { Tabs } from "@/shared/ui/tabs/tabs";
import { useState } from "react";
import { Tab } from "@/shared/ui/tabs/tabs";

const UiKit = () => {
  const someFunc = () => {
    console.log("Hello");
  };

  const tabs: Tab[] = [
    { id: 1, title: "Personal Info" },
    { id: 2, title: "Change password" },
    { id: 3, title: "Password" },
  ];

  const [selectedTabId, setSelectedTabId] = useState(tabs[0].id);

  const handleTabId = (id: string | number) => {
    setSelectedTabId(id);
  };

  return (
    <>
      <Header isAuthUser={false} authUserName="Alex" />
      <section className={styles.uiKit}>
        <h1 className={styles.title}>UI Kit</h1>
        <br />
        <h1 className={styles.title}>Button Primary</h1>
        <div className={styles.container}>
          <ButtonPrimary disabled onClick={someFunc}>
            {"Hello 2!"}
          </ButtonPrimary>
          <ButtonPrimary onClick={someFunc}>{"Hello 2!"}</ButtonPrimary>
          <ButtonPrimary
            isLoading={true}
            onClick={someFunc}
            className={styles.button__loading}
          >
            {"Hello 2!"}
          </ButtonPrimary>
          <ButtonPrimary onClick={someFunc}>{"Hello 2!"}</ButtonPrimary>
          <ButtonPrimary onClick={someFunc}>{"Hello 2!"}</ButtonPrimary>
        </div>

        <h1 className={styles.title}>Button Primary</h1>
        <div className={styles.container}>
          <ButtonSecondary isLoading={true} onClick={someFunc}>
            {"Hello 2!"}
          </ButtonSecondary>
          <ButtonSecondary onClick={someFunc}>{"Hello 2!"}</ButtonSecondary>
          <ButtonSecondary onClick={someFunc} disabled>
            {"Hello 2!"}
          </ButtonSecondary>
        </div>
        <h1 className={styles.title}>Checkboxes</h1>
        <div className={styles.container}>
          <Checkbox id="1" />
          <Checkbox id="2" defaultChecked />
          <Checkbox id="3" disabled />
          <Checkbox id="3" disabled checked />
        </div>
        <h1 className={styles.title}>Inputs</h1>
        <div className={styles.inputs}>
          <Input
            id="4"
            placeholder="Placeholder"
            status={InputStatus.SUCCESS}
          />
          <Input id="4" placeholder="Placeholder" />
          <Input
            id="4"
            placeholder="Placeholder"
            status={InputStatus.ERROR}
            errorMessage="Error text"
          />
          <Input id="4" placeholder="Placeholder" disabled />
          <Input
            id="4"
            placeholder="Placeholder"
            status={InputStatus.ERROR}
            errorMessage="Error text"
          />
        </div>
        <div>
          <Tabs tabs={tabs} selectedId={selectedTabId} onClick={handleTabId} />
          <div className={styles.tab_content}>
            {selectedTabId === tabs[0].id && (
              <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro,
                non id debitis numquam velit minus adipisci maiores magni veniam
                eius impedit sapiente vero provident illum ipsam laudantium
                quaerat cupiditate pariatur?
              </div>
            )}
            {selectedTabId === tabs[1].id && (
              <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
              </div>
            )}
            {selectedTabId === tabs[2].id && <div>ТРИ!</div>}
          </div>
        </div>
      </section>
    </>
  );
};

export default UiKit;
