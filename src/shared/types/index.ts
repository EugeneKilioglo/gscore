export type AnyEvent = MouseEvent | TouchEvent;

export enum InputStatus {
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
  }