import { ButtonHTMLAttributes, FC } from "react";
import { useClasses } from "./lib/use-classes";
import { SvgLoader } from "@/shared/icons/components/loader";
import styles from "./base.module.scss";

export interface BaseProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string;
  contentClassname?: string;
  isLoading?: boolean;
}

export const Base: FC<BaseProps> = ({
  className,
  isLoading = false,
  contentClassname,
  disabled,
  children,
  ...props
}) => {
  const { cnRoot, cnContent } = useClasses({
    className,
    contentClassname,
    isLoading,
  });

  return (
    <button className={cnRoot} disabled={disabled || isLoading} {...props}>
      <span className={cnContent}>{children}</span>
      {isLoading && <SvgLoader />}
    </button>
  );
};