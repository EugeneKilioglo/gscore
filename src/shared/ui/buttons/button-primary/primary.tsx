import { FC } from "react";
import { BaseProps, Base } from "../base/base";

import { useClasses } from "./lib/use-classes";

export const ButtonPrimary: FC<BaseProps> = ({
  className,
  ...props
}) => {
  const { cnRoot } = useClasses({ className });

  return <Base className={cnRoot} {...props} />;
};
