import { SvgChevronDown } from "@/shared/icons/components/chevron-down";
import { FC, useRef } from "react";
import { useClasses } from "./lib/use-classes";
import { useToggle } from "@/shared/hooks/use-toggle";
import { useClickAway } from "@uidotdev/usehooks";
// import { useOutsideClick } from "@/shared/hooks/use-click-outside";

type menuItemsType = {
    title: string;
    icon: JSX.Element;
    onClick: () => void;
}[];

export interface Props {
    title?: string;
    className?: string;
    menuItems: menuItemsType;
}

export const Dropdown: FC<Props> = ({title, className, menuItems, ...props}) => {
    const { isActive, close, toggle } = useToggle();
    const ref = useClickAway(() => {
    close();
    });
    // useOutsideClick(ref,() => close);
    const { cnRoot, cnText, cnIcon, cnDropMenu, cnItemWrapper, cnItemTitle, cnItemIcon, cnButton} = useClasses({
className,
isActive
  });
  return (
    <div className={cnRoot} {...props} ref={ref}>
        <button className={cnButton} onClick={toggle}>
        <div className={cnText}>{title}</div>
        <span className={cnIcon}><SvgChevronDown /></span>
        </button>
        {isActive && 
        <div className={cnDropMenu} >
            {menuItems.map(item => {
                return <button className={cnItemWrapper} onClick={item.onClick}>
                    <span className={cnItemIcon}>{item.icon}</span>
                    <div className={cnItemTitle}>{item.title}</div>
                </button>
            })}
        </div>}
    </div>
  );
};