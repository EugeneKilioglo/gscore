import classNames from "classnames/bind";
import { Props } from "../dropdown";
import classes from "../dropdown.module.scss";

const cn = classNames.bind(classes);

type PickedBaseProps = Pick<
Props,
  "className"
>;

interface UseProps extends PickedBaseProps {
  isActive: boolean;
}

export const useClasses = ({
  className,
  isActive
}: UseProps) => {
  const cnRoot = cn('root', className);
  const cnText = cn('link-title');
  const cnIcon = cn('link-icon', {'link-icon-anim': isActive});
  const cnDropMenu = cn('drop-menu')
  const cnItemWrapper = cn('item');
  const cnItemTitle = cn('item-title');
  const cnItemIcon = cn('item-icon');
  const cnButton = cn('drop-button')

  return {
    cnRoot,
    cnText,
    cnIcon,
    cnDropMenu,
    cnItemWrapper,
    cnItemTitle,
    cnItemIcon,
    cnButton
  };
};