import { FC, InputHTMLAttributes } from "react";
import { SvgCheck } from "@/shared/icons/components/check";
import { useClasses } from "./lib/use-classes";

export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  id: string;
  className?: string;
  checkboxClassname?: string;
  labelClassname?: string;
}

export const Checkbox: FC<Props> = ({ className, checkboxClassname, labelClassname, id, ...props }) => {
  const { cnRoot, cnCheckbox, cnLabel } = useClasses({
    className,
    checkboxClassname,
    labelClassname,
  });

  return (
    <div className={cnRoot}>
      <input type="checkbox" className={cnCheckbox} id={id} {...props} />
      <label htmlFor={id} className={cnLabel}>
        <span>
          <SvgCheck strokeWidth={4} />
        </span>
      </label>
    </div>
  );
};
