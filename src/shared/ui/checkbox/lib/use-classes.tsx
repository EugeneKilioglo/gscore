import classNames from "classnames/bind";
import { Props } from "../checkbox";
import classes from "../checkbox.module.scss";

const cn = classNames.bind(classes);

type PickedBaseProps = Pick<
Props,
  "className" | "checkboxClassname" | "labelClassname"
>;

export const useClasses = ({
  className,
  checkboxClassname,
  labelClassname,
}: PickedBaseProps) => {
  const cnRoot = cn('root', className);

  const cnCheckbox = cn(
    'checkbox',
    checkboxClassname
  );

  const cnLabel = cn(
    'label',
    labelClassname
  );

  return {
    cnRoot,
    cnCheckbox,
    cnLabel
  };
};
