import classNames from "classnames/bind";
import { Props } from "../input";
import classes from "../input.module.scss";

const cn = classNames.bind(classes);

type PickedBaseProps = Pick<
Props,
  "className" | "errorMessage"
>;

interface PropsUseClasses extends PickedBaseProps {
  isError: boolean;
  isSuccess: boolean;
}

export const useClasses = ({
  className,
  isError,
  isSuccess
}: PropsUseClasses) => {
  const cnRoot = cn('root', className);

  const cnInput = cn('input',{ 'input-error': isError,
'input-success': isSuccess }, className );

  const cnErrorText = cn('error-text', className);

  const cnErrorImage = cn('error-image', className);

  const cnSuccessImage = cn('success-image', className);

  return {
    cnRoot,
    cnInput,
    cnErrorText,
    cnErrorImage,
    cnSuccessImage
  };
};
