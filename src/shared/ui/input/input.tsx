import { FC, InputHTMLAttributes } from "react";
import { useClasses } from "./lib/use-classes";
import { InputStatus } from "@/shared/types";
import { SvgCheck } from "@/shared/icons/components/check";
import { SvgClose } from "@/shared/icons/components/close";

export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  id: string;
  className?: string;
  errorMessage?: string;
  status?: InputStatus;
}

export const Input: FC<Props> = ({ className, id, errorMessage, status, ...props }) => {

  const isError = status === InputStatus.ERROR;
  const isSuccess = status === InputStatus.SUCCESS;

  const { cnRoot, cnInput, cnErrorText, cnErrorImage, cnSuccessImage } = useClasses({
    className,
    isError,
    isSuccess
  });
  return (
    <div className={cnRoot}>
      <input type="text" className={cnInput} id={id} {...props} />
      {isError && <span className={cnErrorImage}><SvgClose /></span>}
      {isSuccess && <span className={cnSuccessImage}><SvgCheck /></span>}
      {errorMessage && <span className={cnErrorText}>{errorMessage}</span>}
    </div>
  );
};
