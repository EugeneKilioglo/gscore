import classNames from "classnames/bind";
import { TabsProps } from "../tabs";
import classes from "../tabs.module.scss";

const cn = classNames.bind(classes);

type PickedBaseProps = Pick<TabsProps, "className">;

export const useClasses = ({ className }: PickedBaseProps) => {
  const cnRoot = cn("root", className);
  const cnTab = cn("tab");
  const cnSelectedTab = cn("selectedTab", "tab");

  return {
    cnRoot,
    cnTab,
    cnSelectedTab,
  };
};
