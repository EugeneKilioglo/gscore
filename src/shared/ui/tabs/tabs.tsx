import React, { FC } from "react";
import { useClasses } from "./lib/use-classes";

export interface Tab {
  id: string | number;
  title: string;
}

export interface TabsProps {
  tabs: Tab[];
  selectedId: string | number;
  className?: string;
  onClick: (id: string | number) => void;
}

export const Tabs: FC<TabsProps> = ({
  className,
  selectedId,
  tabs,
  onClick,
}) => {
  const { cnRoot, cnTab, cnSelectedTab } = useClasses({
    className,
  });

  return (
    <div className={cnRoot}>
      {tabs &&
        tabs.map((tab) => (
          <div
            key={tab.id}
            onClick={() => onClick(tab.id)}
            className={tab.id === selectedId ? cnSelectedTab : cnTab}
          >
            <div>{tab.title}</div>
          </div>
        ))}
    </div>
  );
};
