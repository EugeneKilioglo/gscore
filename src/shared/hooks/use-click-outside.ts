import { RefObject, useEffect, useRef } from 'react';
import { AnyEvent } from '@/shared/types';

type Handler = (event?: AnyEvent) => void;

export const useOutsideClick = <T extends HTMLElement>(elementRef: RefObject<T>, handler: Handler) => {
  const handlerRef = useRef<Handler>();
  handlerRef.current = handler;
  const root = document.getElementById('root');

  useEffect(() => {
    const listener = (event: AnyEvent) => {
      const element = elementRef.current;
      const handler = handlerRef.current;

      if (element?.contains(event.target as Node)) {
        return;
      }

      handler?.(event);
      event.stopPropagation();
    };

    if (root) {
      root.addEventListener('click', listener);
      root.addEventListener('touchstart', listener);
    }

    return () => {
      if (root) {
        root.removeEventListener('click', listener);
        root.removeEventListener('touchstart', listener);
      }
    };
  }, [elementRef]);
};
