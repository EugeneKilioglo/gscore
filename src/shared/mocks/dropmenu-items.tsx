import { SvgLogOut } from "../icons/components/log-out";
import { SvgSettings } from "../icons/components/settings";

export const menuItems = [
    {
        title: 'Settings',
        icon: <SvgSettings />,
        onClick: () => console.log('Settings'),
        
    },
    {
        title: 'Logout',
        icon: <SvgLogOut/>,
        onClick: () => console.log('Logout'),
    }
]