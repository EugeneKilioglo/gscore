export const Z_INDEX = {
  hidden: -1,
  base: 1,
  decoractive: 2,
  z10: 10,
  z20: 20,
  z30: 30,
  z40: 40,
  z50: 50,
  dropdown: 99,
  sticky: 200,
  fixed: 1000,
  overlay: 5000,
};
