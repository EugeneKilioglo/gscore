import * as React from 'react';
import { SVGProps } from 'react';
export const SvgAdd = (props: SVGProps<SVGSVGElement>) => (
  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      d="M12 2.63953V20.6395"
      stroke="white"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M3 11.6395H21" stroke="white" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);
