import * as React from 'react';
import { SVGProps } from 'react';
export const SvgClose = (props: SVGProps<SVGSVGElement>) => (
  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M3 21L21 3" stroke="white" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
    <path d="M21 21L3 3" stroke="white" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);
