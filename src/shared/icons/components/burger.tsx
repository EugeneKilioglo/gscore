import * as React from 'react';
import { SVGProps } from 'react';
export const SvgBurger = (props: SVGProps<SVGSVGElement>) => (
  <svg width={40} height={40} viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <rect x={8} y={10} width={24} height={2} rx={1} fill="white" />
    <rect x={8} y={19} width={24} height={2} rx={1} fill="white" />
    <rect x={8} y={28} width={24} height={2} rx={1} fill="white" />
  </svg>
);
