import * as React from 'react';
import { SVGProps } from 'react';
export const SvgIconButton = (props: SVGProps<SVGSVGElement>) => (
  <svg width={26} height={26} viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <rect width={26} height={26} rx={13} fill="white" />
    <path
      d="M12.9998 13L16.5353 16.5355M9.46424 16.5355L12.9998 13L9.46424 16.5355ZM16.5353 9.46447L12.9998 13L16.5353 9.46447ZM12.9998 13L9.46424 9.46447L12.9998 13Z"
      stroke="#B9B9B9"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
