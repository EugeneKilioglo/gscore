import * as React from 'react';
import { SVGProps } from 'react';
export const SvgLoading = (props: SVGProps<SVGSVGElement>) => (
  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <circle opacity={0.5} cx={12} cy={12} r={9} stroke="#B9B9B9" strokeWidth={2} />
    <path d="M12 21C7.02944 21 3 16.9706 3 12" stroke="#B9B9B9" strokeWidth={2} />
  </svg>
);
